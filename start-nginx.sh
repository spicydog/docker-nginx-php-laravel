#!/usr/bin/with-contenv sh
set -e;

/bin/wait-for-it.sh -t 120 127.0.0.1:9000

sed -i.bak "s/8080/$PORT/g" /etc/nginx/nginx.conf

/usr/sbin/nginx
